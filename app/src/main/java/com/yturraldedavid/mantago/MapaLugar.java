package com.yturraldedavid.mantago;

public class MapaLugar {
    private double Lat;
    private double Lng;



    public MapaLugar(double Lat, double Lng){
        this.Lat = Lat;
        this.Lng = Lng;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLng() {
        return Lng;
    }

    public void setLng(double lng) {
        Lng = lng;
    }
}
